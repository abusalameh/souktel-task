export function clearQueryString (url, key) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(key)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }
        url= urlparts[0]+'?'+pars.join('&');
        return url;
    } else {
        return url;
    }
}


export function getPageNumberFromRoute(){
    var currentPage = "";
    // Get URL from the browser
    var currentUrl = window.location.href;
    // search for 'page' in the string
    var index = currentUrl.indexOf('page');
    // extract the page number after 'page/' 
    currentPage = (index !== -1) ? currentUrl.substr(currentUrl.indexOf('page')+5,currentUrl.length) : "";
    // return page number if there's , otherwise return 1
    return currentPage.length ? currentPage : 1;
}