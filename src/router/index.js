import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/List'
import User from '@/components/User'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '*',
      name: 'users-page',
      component: List
    },
    
    {
      path: '/',
      name: 'users',
      component: List
    }
  ]
})
