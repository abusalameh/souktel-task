// Import Vue, Vue-Router, Axios and Bootstrap-Vue
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VModal from 'vue-js-modal'

Vue.use(VModal)
Vue.use(BootstrapVue);

Vue.config.productionTip = false

// Init the App
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
